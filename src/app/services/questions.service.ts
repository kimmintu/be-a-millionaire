import { Injectable } from '@angular/core';
import { Question } from '../models/question';
import { AngularFire, FirebaseListObservable } from 'angularfire2';

@Injectable()
export class QuestionsService {
    private items: FirebaseListObservable<Question[]>;
    constructor(private af: AngularFire) {
        this.items = af.database.list('questions');
    }

    fetch(): Promise<Question[]> {
        let promise = new Promise<Question[]>((resolve, reject) => {
            let subscription = this.items.subscribe((questions: Question[]) => {
                resolve(questions);
                subscription.unsubscribe();
            });
        });

        return promise;
    }

    remove(key: string): void {
        this.items.remove(key);
    }
}