import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Game } from '../models/game';
import { Person } from '../models/person';
import { Audience } from '../models/audience';

@Injectable()
export class FirebaseService {

    constructor(private af: AngularFire) {        
    }

    createNewGame(moderator: Person) {
        let game = new Game();
        game.moderator = moderator;
        game.currentLevel = 0;
        game.dateTime = (new Date()).getTime();
        return this.af.database.list('games').push(game);
    }

    createNewGameRef(newGameRef: string) {
        return this.af.database.object('newGameRef').set({key: newGameRef});
    }

    getNewGameRef() {
        return this.af.database.object('newGameRef');
    }

    getChallengeQuestion(gameKey: string) {
        console.log("games/" + gameKey);
        return this.af.database.object('games/' + gameKey + '/challengeQuestion');
    }

    createNewAudience(audience: Person, gameKey: string) {
        return this.af.database.list('games/' + gameKey + '/audience').push(audience);
    }

    updateAudienceAnswer(audience: Audience, gameKey: string) {
        
    }

}