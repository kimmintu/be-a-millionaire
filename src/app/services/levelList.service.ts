import { Injectable } from '@angular/core';
import { QuestionsService } from './questions.service';
import { Level } from '../models/level';
import { Question } from '../models/question';

export class LevelListService {
    constructor(private questionsService: QuestionsService) { }

    getList(): Promise<Array<Level>> {
        let sortedList: Array<Level> = this.getEmptyQuestionsList();

        let promise = new Promise<Array<Level>>((resolve, reject) => {
            this.questionsService.fetch().then((questions: Question[]) => {
                questions.forEach((question: Question) => {
                    sortedList[parseInt(question.level as any, 10)].questions.push(question);
                });

                resolve(sortedList);
            });
        });

        return promise;
    }

    private getEmptyQuestionsList(): Array<Level> {
        let result: Array<Level> = [];

        for (let i = 0; i <= 10; i++) {
            result[i] = {
                index: i,
                questions: []
            }
        }

        return result;
    }
}