import { Routes } from '@angular/router';
import { QuestionsListComponent } from './pages/questions/questionsList.component';
import { QuestionFormComponent } from './pages/questions/questionForm.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { StartGameComponent } from './pages/start-game/start-game.component';
import { ChallengeQuestionComponent } from './pages/challenge-question/challenge-question.component';
import { AppComponent } from './app.component';

export const appRoutes: Routes = [
  { path: 'questions', component: QuestionsListComponent },
  { path: 'questions/add', component: QuestionFormComponent },
  { path: 'questions/edit/:$key', component: QuestionFormComponent },
  { path: 'dashboard/:gameId', component: DashboardComponent },
  { path: 'audience/:gameId/:userId', component: ChallengeQuestionComponent },
  { path: 'role/:role', component: StartGameComponent },
];;