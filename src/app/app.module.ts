import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { RouterModule, Routes } from '@angular/router';
import { appRoutes } from './routes';
import { FirebaseService } from './services/firebase.service';
import { StrategyKeeper } from './pages/questions/questionStrategies/strategyKeeper';
import { QuestionsListComponent } from './pages/questions/questionsList.component';
import { QuestionFormComponent } from './pages/questions/questionForm.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { QuestionsService } from './services/questions.service';
import { StartGameComponent } from './pages/start-game/start-game.component';
import { AppComponent } from './app.component';
import { ChallengeQuestionComponent } from './pages/challenge-question/challenge-question.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyCqG4EXyRIO1vrwpQVuGN7NxQ97h9Ig9AQ',
  authDomain: 'who-become-a-milloner.firebaseapp.com',
  databaseURL: 'https://who-become-a-milloner.firebaseio.com',
  storageBucket: 'who-become-a-milloner.appspot.com',
  messagingSenderId: '793699021621'
};

@NgModule({
  declarations: [
    AppComponent,
    QuestionsListComponent,
    QuestionFormComponent,
    DashboardComponent,
    StartGameComponent,
    ChallengeQuestionComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [StrategyKeeper, QuestionsService, FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
