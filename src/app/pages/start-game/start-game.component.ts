import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Person } from '../../models/person';
import { FirebaseService } from '../../services/firebase.service';
import { AngularFire } from 'angularfire2';


@Component({
    selector: 'start-game',
    templateUrl: './start-game.component.html',
    styleUrls: ['./start-game.component.css']
})

export class StartGameComponent implements OnInit {
    startGameForm: FormGroup;
    person: Person = new Person();
    role;

    constructor(
            fb: FormBuilder, private fs: FirebaseService, 
            private router: Router, private route: ActivatedRoute
        ) {
        this.startGameForm = fb.group({
            name: ['', Validators.required],
            email: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                if (params["role"] === "moderator") {
                    this.role = "Moderator";
                } else {
                    this.role = "Audience";
                }
            }
        );
    }

    startGame() {
        console.log("startgame: role = " + this.role);
        if (this.role.toLowerCase() === "moderator") {
            this.fs.createNewGame(this.person).then(newGameRef => {
                this.fs.createNewGameRef(newGameRef.key).then(donotcare => {
                    this.router.navigate(['/dashboard', newGameRef.key]);
                })                
            });
        } else {
            this.fs.getNewGameRef().subscribe(newGameRef => {
                this.fs.createNewAudience(this.person, newGameRef.key).then(donotcare => {
                    this.router.navigate(['/audience', newGameRef.key, donotcare.key]);
                });
            })            
        }
        
    }

}
