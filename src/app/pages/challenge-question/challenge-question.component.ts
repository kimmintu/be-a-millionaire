import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseService } from '../../services/firebase.service';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Question } from '../../models/question';
import { Audience } from '../../models/audience';

@Component({
  selector: 'app-challenge-question',
  templateUrl: './challenge-question.component.html',
  styleUrls: ['./challenge-question.component.css']
})
export class ChallengeQuestionComponent implements OnInit {

  challengeQuestion;
  answers = [];

  constructor(private fbService: FirebaseService, private route: ActivatedRoute, private af: AngularFire) {

  }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.fbService.getChallengeQuestion(params["gameId"]).subscribe(
          question => {
            this.challengeQuestion = question;
            this.answers = this.shuffle(this.toArray(question.answers)); 
          }
        );
      }
    );
  }

  public selectAnswer(event: MouseEvent) {
    let user = this.af.database.object('games/' + this.route.params['value']['gameId'] + '/audience/' + this.route.params['value']['userId']);
    user.subscribe((record: any) => {
      let audience = new Audience();
      audience.answer = {
        index: event.target['dataset']['key'],
        time: new Date()
      };
      audience.name = record.name;
      audience.email = record.email;

      user.update(audience);
    });
  }

  private toArray(answers) {
    return [{
      isCorrect: true,
      answer: answers.correct
    }, { 
      isCorrect: false,
      answer: answers.incorrect1
    },{
      isCorrect: false,
      answer: answers.incorrect2
    }, {
      isCorrect: false,
      answer: answers.incorrect3
    }];
  }

  private shuffle(a) {
    var j, x, i;
    for (i = a.length; i; i--) {
        j = Math.floor(Math.random() * i);
        x = a[i - 1];
        a[i - 1] = a[j];
        a[j] = x;
    }
    return a;
}
}
