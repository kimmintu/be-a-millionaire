import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Component } from '@angular/core';
import { QuestionsService } from '../../services/questions.service';
import { LevelListService } from '../../services/levelList.service';
import { IQuestion, Question } from '../../models/question';
import { Level } from '../../models/level';
import { Person } from '../../models/person';
import { Game } from '../../models/game';
import { QuestionsListComponent } from '../questions/questionsList.component'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent {
    private questionsList: Array<Level>
    private levelListService: LevelListService;
    private audienceLink: string;
    public usersList: Array<Person>;
    private gameId: string;
    public game: Game;
    public explanation: string;

    constructor(private questionsService: QuestionsService, currentRoute: ActivatedRoute,
        private af: AngularFire, router: Router) {
        this.gameId = currentRoute.params['value']['gameId'];

        let gameSubcription = af.database.object(`/games/${this.gameId}`).subscribe((game: any) => {
            if (!game.$exists()) {
                router.navigate(['/']);
            } else {
                this.renderQuestionList(questionsService);
                this.listenForUsers(af);
                this.createLinkForAudience();
                this.game = game;
            }
            gameSubcription.unsubscribe();
        });
    }

    private createLinkForAudience() {
        this.audienceLink = `${window.location.origin}/audience/${this.gameId}`;
    }

    public pickQuestion(event: any) {
        let currentQuestionDoc = this.af.database.object(`/games/${this.gameId}/challengeQuestion`);
        let question = this.findQuestion(event.target.dataset.key);
        currentQuestionDoc.set(question);
        event.target.disabled = true;
        this.explanation = question.explanation;

    }

    private findQuestion(key: string): Question {
        let questionsOnLevel = this.questionsList[this.game.currentLevel].questions;
        let question = questionsOnLevel.find((question: IQuestion) => {
            return question.$key === key;
        });

        return new Question(question.level, question.title, question.answers, question.explanation);
    }

    private listenForUsers(af: AngularFire) {
        let users = af.database.list(`/games/${this.gameId}/audience`);
        this.usersList = [];

        users.subscribe((users: Person[]) => {
            this.usersList = users;
        });
    }

    private renderQuestionList(questionsService: QuestionsService): void {
        this.questionsList = [];
        let levelListService = new LevelListService(questionsService);

        levelListService.getList().then((sortedList: Array<Level>) => {
            this.questionsList = sortedList;
        });
    }
}
