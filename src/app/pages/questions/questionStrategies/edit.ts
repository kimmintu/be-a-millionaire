import { Strategy } from './strategyKeeper';
import { Question } from '../../../models/question';
import { Router } from '@angular/router';
import { AngularFire } from 'angularfire2';

export class Edit implements Strategy {
    private question: any;

    constructor(private af: AngularFire, private params: any) {}

    public getQuestion(): Promise<Question> {
        let questionKey = this.params['value']['$key'];
        this.question = this.af.database.object(`questions/${questionKey}`);

        let promise = new Promise<Question>((resolve, reject) => {
            let subscription = this.question.subscribe((question: Question) => {
                resolve(new Question(question.level, question.title,
                    question.answers, question.explanation));
                subscription.unsubscribe();
            });
        });

        return promise;
    }

    public onSubmit(model: Question) {
        this.question.update(model);
    }
}