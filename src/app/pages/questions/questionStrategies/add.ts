import { Strategy } from './strategyKeeper';
import { Question } from '../../../models/question';
import { Router } from '@angular/router';
import { AngularFire } from 'angularfire2';

export class Add implements Strategy {
    
    constructor(private af: AngularFire) {}
    public getQuestion(): Promise<Question> {
        let promise = new Promise<Question>((resolve, reject) => {
            resolve(new Question());
        });

        return promise;
    }

    public onSubmit(model: Question) {
        let questions = this.af.database.list('questions');
        questions.push(model);
    }
}