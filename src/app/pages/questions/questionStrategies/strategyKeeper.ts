import { Injectable } from '@angular/core';
import { Add } from './add';
import { Edit } from './edit';
import { Question } from '../../../models/question';
import { AngularFire } from 'angularfire2';

let strategies = {
    'add': Add,
    'edit': Edit
}
export interface Strategy {
    getQuestion: () => Promise<Question>;
    onSubmit: (model: Question) => void;
}
@Injectable()
export class StrategyKeeper {
    initStrategy(af: AngularFire, params: any, name: string): Strategy {
        return new strategies[name](af, params);
    }
}