import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { Question } from '../../models/question';
import { StrategyKeeper, Strategy } from './questionStrategies/strategyKeeper';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './questionForm.component.html',
  styleUrls: ['./questionForm.component.css'],
})
export class QuestionFormComponent {
    model: Question;
    private strategy: Strategy;
    constructor(private af: AngularFire, strategyKeeper: StrategyKeeper, private route: Router, private currentRoute: ActivatedRoute) {
        let strategyName = this.currentRoute.url['getValue']()[1];
        this.strategy = strategyKeeper.initStrategy(af, this.currentRoute.params, strategyName);
        this.strategy.getQuestion().then((question: Question) => {
            this.model = question;
        });

        this.model = new Question();
    }

    public onSubmit() {
        this.strategy.onSubmit(this.model);
        this.route.navigateByUrl('/questions');
    }
}