import { Component } from '@angular/core';
import { Level } from '../../models/level';
import { Question } from '../../models/question';
import { QuestionsService } from '../../services/questions.service';
import { LevelListService } from '../../services/levelList.service';

@Component({
  selector: 'questions-list',
  templateUrl: './questionsList.component.html',
  styleUrls: ['./questionsList.component.css'],
})
export class QuestionsListComponent {
  levels: Array<Level>;
  private levelListService: LevelListService;
  constructor(private questionsService: QuestionsService) {
    this.levelListService = new LevelListService(questionsService);
    this.renderQuestionList();
  }

  private renderQuestionList(): void {
    this.levels = [];

    this.levelListService.getList().then((sortedList: Array<Level>) => {
      this.levels = sortedList;
    });
  }

  public removeQuestion(event: any): void {
    this.questionsService.remove(event.target.dataset.key);

    this.renderQuestionList();
  }
}