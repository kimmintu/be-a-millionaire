import { Question } from './question';

export interface Level {
  index: number;
  questions: Array<Question>
}