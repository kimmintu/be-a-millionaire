import { Person } from './person';

export interface Answer {
    index: number;
    time: Date;
}
export class Audience extends Person {
    answer: Answer;
}