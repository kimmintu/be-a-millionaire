export interface Answers {
    index: number;
    text: string;
}
export interface Answers {
    correct: Answers;
    incorrect1: Answers;
    incorrect2: Answers;
    incorrect3: Answers;
}

export interface IQuestion {
    $key?: string;
    level: number;
    title: string;
    answers: Answers;
    explanation: string;
}

export class Question {
    constructor(public level = 1, 
        public title = '',
        public answers = {
            correct: {
                index: 1,
                text: ''
            },
            incorrect1: {
                index: 2,
                text: ''
            },
            incorrect2: {
                index: 3,
                text: ''
            },
            incorrect3: {
                index: 4,
                text: ''
            }
        }, 
        public explanation = '') {}
}