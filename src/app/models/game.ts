import { Person } from './person';

export class Game {
    dateTime: number;
    moderator: Person;
    player: Person;
    currentLevel: number;
    audience: Array<Person>;
}