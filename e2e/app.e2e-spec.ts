import { BeAMillionairePage } from './app.po';

describe('be-a-millionaire App', function() {
  let page: BeAMillionairePage;

  beforeEach(() => {
    page = new BeAMillionairePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
